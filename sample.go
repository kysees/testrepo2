package testrepo2

import "fmt"

type Bitbucket struct {}

func (b *Bitbucket) Hello() {
    fmt.Println("Hello. I'm in Bitbucket.")
}